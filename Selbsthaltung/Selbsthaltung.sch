EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FINDER-40.31 K1
U 1 1 5B05C8FD
P 5400 3700
F 0 "K1" H 5850 3500 50  0000 L CNN
F 1 "24V Relais" H 5850 3400 50  0000 L CNN
F 2 "Relays_THT:Relay_SPDT_Finder_40.31" H 6540 3660 50  0001 C CNN
F 3 "" H 5400 3700 50  0001 C CNN
	1    5400 3700
	1    0    0    -1  
$EndComp
$Comp
L SW_SPST_Lamp SW1
U 1 1 5B05F6FB
P 6900 3650
F 0 "SW1" H 6925 3875 50  0000 L CNN
F 1 "Hutschienen Taster" H 6900 3525 50  0000 C CNN
F 2 "" H 6900 3950 50  0001 C CNN
F 3 "" H 6900 3950 50  0001 C CNN
	1    6900 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5600 4000 5600 4350
Wire Wire Line
	4700 4350 5600 4350
Wire Wire Line
	5600 4350 7800 4350
Wire Wire Line
	4700 3100 4700 3250
Wire Wire Line
	4700 3250 4700 4350
Wire Wire Line
	4700 3250 5200 3250
Wire Wire Line
	5200 3250 5200 3400
Wire Wire Line
	5700 2950 5700 3200
Wire Wire Line
	5700 3200 5700 3400
Wire Wire Line
	5700 3200 6350 3200
Wire Wire Line
	6350 3200 6350 3550
Wire Wire Line
	6350 3550 6700 3550
Connection ~ 5700 3200
Wire Wire Line
	7100 3550 7800 3550
Wire Wire Line
	7800 3550 7800 4350
Connection ~ 5600 4350
$Comp
L +24V #PWR2
U 1 1 5B05F832
P 5700 2950
F 0 "#PWR2" H 5700 2800 50  0001 C CNN
F 1 "+24V" H 5700 3090 50  0000 C CNN
F 2 "" H 5700 2950 50  0001 C CNN
F 3 "" H 5700 2950 50  0001 C CNN
	1    5700 2950
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR1
U 1 1 5B05F8A8
P 5200 4100
F 0 "#PWR1" H 5200 3850 50  0001 C CNN
F 1 "GND" H 5200 3950 50  0000 C CNN
F 2 "" H 5200 4100 50  0001 C CNN
F 3 "" H 5200 4100 50  0001 C CNN
	1    5200 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 4000 5200 4100
Text GLabel 4700 3100 0    39   Input ~ 0
Pin_15_16-pol.-Stecker
Connection ~ 4700 3250
NoConn ~ 5500 3400
Text GLabel 6700 3650 0    39   Input ~ 0
230VAC_Leiter
Text GLabel 7100 3650 2    39   Input ~ 0
230VAC_Neutralleiter
$EndSCHEMATC
